# Clipper

Clipper is a Python `3.7x` based clipboard manager. This tool is being build in a personal effort to learn more about the Python language in general, and to make a decent, universal clipboard manager (à la Ditto on Windows). 

-----------

### _**Current Release**_: `1.1` **(Alpha)**

The current working release of Clipper is very bare-bones, as _this is just a working concept_. Future implementations include many handy features, such as a search functionality, automated DB cleanup, keyboard shortcuts & more. Feature proposals can be submitted via an Issue.

-----------

### Important Details 

The current release of Clipper requires manual execution of the `clip.py` script, as well as the installation of the `pyperclip` library (run `pip install pyperclip`). Once the script is executed, any new clipboard entries will be automatically stored to a local SQLite DB (this is generated on script-startup & persists until manually removed). 

**Once a new entry is copied, the GUI will be updated with the new clipping within 1 second**. Clicking the checkbox next to each clipping will automatically replace your clipboards current value, making the desired clipping ready to be pasted. 
