import tkinter as tk
from tkinter import *
import pyperclip, sqlite3

# Tkinter definition and size
window = tk.Tk()
window.title("Clipper :)")
window.geometry("400x400")

# SQLite variable definitions
sql_file = "clippy_db.sqlite"
tbl_1 = "clips_taken"
col="clip"
field_type="TEXT"

# SQLite connection
conn = sqlite3.connect(sql_file)
conn.text_factory = str
c = conn.cursor()

# Create SQLite database if not exist
c.execute("CREATE TABLE IF NOT EXISTS {tn} ({col} {ft}, UNIQUE ({col}))"\
            .format(tn=tbl_1, col=col, ft=field_type))

# Add clips to the SQLite database
def addClips():
    newClip = pyperclip.paste()
    c.execute("INSERT OR IGNORE INTO {tn} ({col}) VALUES ('{val}')"\
                .format(tn=tbl_1, col=col, val=newClip))
    conn.commit()
    window.after(ms=100, func=addClips)
addClips()

# Dynamically create radio buttons containing clipped values
v = tk.IntVar()
v.set(1)
buttList = []
def allClips():
    c.execute("SELECT * FROM {tn}".format(tn=tbl_1))
    results = c.fetchall()
    for val, item in enumerate(results):
        butt = tk.Radiobutton(window, text=item[0], command=lambda x=item[0]: pyperclip.copy(x),variable=v, value=val) 
        butt.pack(anchor=tk.W)
        buttList.append(butt)

# Delete old buttons and create new ones with updated DB values
def reloadButt():
    for rad in buttList:
        rad.destroy()
    allClips()
    window.after(ms=1000, func=reloadButt)

# Generate buttons & then delete them, this allows the reloaded buttons to automatically update. DO NOT MOVE/EDIT
allClips()
reloadButt()

window.mainloop()